package ru.t1.gorodtsova.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.gorodtsova.tm.model.AbstractModel;

import java.util.Collection;
import java.util.List;

public interface IService<M extends AbstractModel> {

    @NotNull
    @Transactional
    M add(M model);

    @NotNull
    @Transactional
    Collection<M> add(@Nullable Collection<M> models);

    @NotNull
    @Transactional
    Collection<M> set(@Nullable Collection<M> models);

    @Transactional
    void update(M model);

    @NotNull
    List<M> findAll();

    M findOneById(@Nullable String id);

    @Transactional
    void removeAll();

    @Transactional
    void removeAll(@Nullable Collection<M> collection);

    @Transactional
    void removeOne(M model);

    @Transactional
    void removeOneById(@Nullable String id);

    boolean existsById(@Nullable String id);

    long getSize();

}
