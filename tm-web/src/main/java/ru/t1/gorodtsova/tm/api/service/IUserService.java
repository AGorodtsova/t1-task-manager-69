package ru.t1.gorodtsova.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.enumerated.RoleType;
import ru.t1.gorodtsova.tm.model.User;

import java.util.List;

public interface IUserService {

    void create(
            @Nullable String login,
            @Nullable String password,
            @Nullable RoleType roleType);

    void save(@Nullable User user);

    @Nullable
    List<User> findAll();

    @Nullable
    User findOneById(@Nullable String id);

    @Nullable
    User findByLogin(@Nullable String login);

    void remove(@Nullable User user);

    void removeById(@Nullable String id);

    boolean existsByLogin(@Nullable String login);

}
