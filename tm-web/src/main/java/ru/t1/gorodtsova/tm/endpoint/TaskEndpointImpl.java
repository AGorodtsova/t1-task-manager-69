package ru.t1.gorodtsova.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.t1.gorodtsova.tm.api.service.ITaskService;
import ru.t1.gorodtsova.tm.model.Task;
import ru.t1.gorodtsova.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@RestController
@RequestMapping("api/tasks")
public class TaskEndpointImpl {

    @Autowired
    private ITaskService taskService;

    @WebMethod
    @GetMapping("/findAll")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public List<Task> findAll() {
        return taskService.findAllByUserId(UserUtil.getUserId());
    }

    @WebMethod
    @GetMapping("/findById/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public Task findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        return taskService.findByIdAndUserId(id, UserUtil.getUserId());
    }

    @WebMethod
    @PostMapping("/save")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public void save(
            @WebParam(name = "task", partName = "task")
            @RequestBody Task task
    ) {
        taskService.saveByUserId(task, UserUtil.getUserId());
    }

    @WebMethod
    @PostMapping("/deleteById/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        taskService.removeByIdAndUserId(id, UserUtil.getUserId());
    }

    @WebMethod
    @PostMapping("/delete")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public void delete(
            @WebParam(name = "task", partName = "task")
            @RequestBody Task task
    ) {
        taskService.removeByUserId(task, UserUtil.getUserId());
    }

}
