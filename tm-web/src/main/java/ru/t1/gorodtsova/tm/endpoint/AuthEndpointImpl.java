package ru.t1.gorodtsova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.t1.gorodtsova.tm.api.service.IUserService;
import ru.t1.gorodtsova.tm.model.Result;
import ru.t1.gorodtsova.tm.model.User;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;

@RestController
@RequestMapping("/api/auth")
public class AuthEndpointImpl {

    @Resource
    private AuthenticationManager authenticationManager;

    @Autowired
    private IUserService userService;

    @WebMethod
    @PostMapping("/login")
    public Result login(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password
    ) {
        try {
            final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
            final Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return new Result(authentication.isAuthenticated());
        } catch (final Exception e) {
            return new Result(e);
        }
    }

    @WebMethod
    @GetMapping("/profile")
    public User profile() {
        @NotNull final SecurityContext securityContext = SecurityContextHolder.getContext();
        @NotNull final Authentication authentication = securityContext.getAuthentication();
        @NotNull final String login = authentication.getName();
        return userService.findByLogin(login);
    }

    @PostMapping("/logout")
    public Result logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return new Result();
    }

}
