package ru.t1.gorodtsova.tm;

import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.gorodtsova.tm.api.IPropertyService;
import ru.t1.gorodtsova.tm.component.Bootstrap;
import ru.t1.gorodtsova.tm.configuration.LoggerConfiguration;
import ru.t1.gorodtsova.tm.listener.EntityListener;
import ru.t1.gorodtsova.tm.service.LoggerService;
import ru.t1.gorodtsova.tm.service.PropertyService;

import javax.jms.*;

public final class ConsumerApplication {

    @SneakyThrows
    public static void main(@Nullable final String[] args) {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(LoggerConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.init();
    }

}
